var emails = ['prachi@consultadd.com',
'shreya@consultadd.com',
'dimple@consultadd.com',
'devesh@consultadd.com',
'bhavesh@consultadd.com']

const url = "https://demo7857661.mockable.io/testdata";


function start(){
    var form = document.createElement("form");
    document.body.appendChild(form);
    document.body.style.backgroundColor="#F3ECF6";
    document.body.style.padding="100px";

    form.id="contact1";
    form.setAttribute('name',"contact")
    form.setAttribute('action',"")
    form.setAttribute('method','post');
    var div1 = document.createElement("div");
    div1.setAttribute('class','divclass');
    form.appendChild(div1);
    var label=document.createElement("label");
    label.innerHTML="Enter email id: <br>"
    label.style.marginLeft="700px";
    label.style.fontSize="20px";
    div1.appendChild(label);
    var input_email = document.createElement("input");
    input_email.setAttribute('placeholder', 'Email');
    input_email.setAttribute('type', 'email');
    input_email.setAttribute('name', 'email');
    input_email.setAttribute('id', 'email');
    input_email.setAttribute('onfocus', 'changeBg(this)');
 
    div1.appendChild(input_email);
    document.getElementById("email").style.backgroundColor="#E4ECF1";
    document.getElementById("email").style.padding="10px";
    document.getElementById("email").style.fontSize="20px";
    document.getElementById("email").style.borderColor="#31393E";
    document.getElementById("email").style.marginTop="20px";
    document.getElementById("email").style.marginBottom="20px";
    document.getElementById("email").style.marginLeft="620px";
    var span1 = document.createElement("span");
    span1.style.display="none";
    span1.setAttribute('id','span1');
    span1.style.marginBottom="15px";
    div1.appendChild(span1);
    var span2 = document.createElement("span");
    span2.style.display="none";
    span2.setAttribute('id','span1');
    span2.style.marginBottom="10px";
    div1.appendChild(span2);

    var div2 = document.createElement("div");
    div2.setAttribute('class','divclass');
    form.appendChild(div2);
    var submit = document.createElement("button");
    submit.innerHTML="Send"
    submit.setAttribute('type','submit');
    submit.setAttribute('name','submit');
    submit.setAttribute('id','submit');
    div2.appendChild(submit);  
    document.getElementById("submit").style.marginLeft="680px";
    document.getElementById("submit").style.padding="10px";
    document.getElementById("submit").style.width="150px";
    document.getElementById("submit").style.backgroundColor="#C70039 ";
    document.getElementById("submit").style.fontSize="20px";
    document.getElementById("submit").style.color="#FFFFFF";
    document.getElementById("submit").style.borderRadius="25px";


    document.getElementById("submit").addEventListener("click", function(event) {
        var email = document.getElementById('email').value;
        console.log(email);
        
        var emailPattern = /^[a-zA-Z0-9._-]+@[a-zA-Z0-9.-]+\.[a-zA-Z]{2,4}$/;
        if(emailPattern.test(email)){
            
            var domain = email.split('@').pop()  // split on '@' and get the last item
            console.log(domain);
            var txt = 'The domain is <b>'+domain+'</b>';
            span2.innerHTML=txt;
            span2.style.marginLeft="670px";
            span2.style.display="block";

            if (emails.indexOf(email) !== -1){
                span1.innerHTML="Email is in the record!";
                span1.style.marginLeft="690px";
                span1.style.display="block";

                //alert("Email exists in the record!");

            }else{
                span1.style.marginLeft="640px";
                span1.innerHTML="Email is valid but not in the record!";
                span1.style.display="block";
                //alert("Email is valid but it is not in record!");
            }
        }else{
            span1.style.marginLeft="680px";
            span1.innerHTML="Sorry...Email is invalid.";
            span1.style.display="block";
            //alert("Email is invalid");
        }

    fetch(url)
        .then((response) => {
            return response.json();
        })
    .then((data) => {
        console.log("Fetch Data ");
        window.localStorage.setItem('name',data.firstName);
        var age1=data.age;
        sessionStorage.setItem('age',age1);
        window.location.href = "home.html";
    })
    .catch(() => console.log("Can’t access " + url + " response. Blocked by browser?"));

    
    event.preventDefault();

    });

}

function changeBg(x) {
    x.style.background = "#F7D0DB";
}

